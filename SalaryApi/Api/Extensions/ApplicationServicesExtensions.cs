﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using SalaryApi.Aplication;
using SalaryApi.Infrastructure;

namespace SalaryApi.Api
{
    public static class ApplicationServicesExtensions 
    {
        public static IServiceCollection AddApplicationServices(
            this IServiceCollection services, 
            IConfiguration configurations)
        {
            services.AddTransient<ISalaryRepository, SalaryRepository>();
            services.AddTransient<ISalaryService, SalaryService>();

            services.AddTransient<IOfficeRepository, OfficeRepository>();
            services.AddTransient<IPositionRepository, PositionRepository>();
            services.AddTransient<IDivisionRepository, DivisionRepository>();

            services.AddDbContext<DataBaseContext>(options => {
                options.UseSqlServer(configurations.GetConnectionString("DefaultConnection"));
            });

            return services; 
        }
    }
}
