﻿using Microsoft.AspNetCore.Mvc;
using SalaryApi.Api.DTOs;
using SalaryApi.Aplication;
using SalaryApi.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace SalaryApi.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SalaryController : ControllerBase
    {
        private readonly ISalaryService _salaryService;

        public SalaryController(ISalaryService salaryService)
        {
            _salaryService = salaryService;
        }

        [HttpPost("add-salaries")]
        public async Task<ActionResult<bool>> AddSalaries(SalaryInputDto salaries)
        {
            if(await _salaryService.AddSalariesAsync(salaries))
            {
                return Ok(true);
            };
            throw new Exception("the server was unable to save the salaries");
        }

        [HttpGet("all")]
        public async Task<ActionResult<IEnumerable<SalaryOutputDto>>> GetSalariesAsync()
        {
            var salaries = await _salaryService.GetSalariesAsync();

            return Ok(salaries);
        }

        [HttpGet("employees-info")]
        public async Task<ActionResult<IEnumerable<string>>> GetEmployeesInfo()
        {
            return Ok(await _salaryService.GetEployeeInfoAsync());
        }

        [HttpGet("available-months")]
        public async Task<ActionResult<IEnumerable<int>>> GetAvailableMonths([FromQuery] AvailableMonthInputDto input)
        {
            return Ok(await _salaryService.GetAvaliableMonths(input));
        }
    }
}