﻿using System;

namespace SalaryApi.Api.DTOs
{
    public class SalaryOutputDto
    {
        public string EmployeeCode { get; set; }
        public string EmployeeFullName { get; set; }
        public string DivisionName { get; set; }    
        public string PositionName { get; set; }
        public int Grade { get; set; }
        public DateTime BeginDate { get; set; }
        public DateTime BirthDate { get; set; }
        public string IdentificationNumber { get; set; }
        public decimal TotalSalary { get; set; }
        public int OfficeId { get; set; }

    }
}
