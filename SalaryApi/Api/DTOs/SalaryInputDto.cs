﻿using System;
using System.Collections.Generic;

namespace SalaryApi.Api.DTOs
{
    public class SalaryInputDto
    {
        public string EmployeeName { get; set; }
        public string EmployeeSurName { get; set; }
        public string IdentificationNumber { get; set; }
        public string EmployeeCode { get; set; }
        public DateTime BeginDate { get; set; }
        public DateTime Birthdate { get; set; }
        public IEnumerable<VariableData> VariableData { get; set; }

        /*Variable Data: ********************/
        
    }

    public class VariableData
    {
        public int Year { get; set; }
        public int Month { get; set; }
        public int OfficeId { get; set; }
        public int DivisionId { get; set; }
        public int PositionId { get; set; }
        public int Grade { get; set; }
        public decimal BaseSalary { get; set; }
        public decimal ProductionBonus { get; set; }
        public decimal CompensationBonus { get; set; }
        public decimal Commission { get; set; }
        public decimal Contributions { get; set; }
    }
}
