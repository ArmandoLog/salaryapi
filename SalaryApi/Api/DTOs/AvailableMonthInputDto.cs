﻿namespace SalaryApi.Api.DTOs
{
    public class AvailableMonthInputDto
    {
        public string EmployeeName { get; set; }
        public string EmployeeSurName { get; set; }
        public int Year { get; set; }
    }
}
