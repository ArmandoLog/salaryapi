﻿using System;

namespace SalaryApi.Api.DTOs
{
    public class EmployeeInfoDto
    {
        public string EmployeeName { get; set; }
        public string EmployeeSurname { get; set; }
        public string EmployeeCode { get; set; }
        public string IdentificationNumber { get; set; }
        public DateTime Birthdate { get; set; }
        public DateTime BeginDate { get; set; }

    }
}
