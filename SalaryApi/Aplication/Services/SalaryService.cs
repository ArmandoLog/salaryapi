﻿
using SalaryApi.Api.DTOs;
using SalaryApi.Domain.Entities;
using SalaryApi.Infrastructure;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SalaryApi.Aplication
{
    public class SalaryService :    ISalaryService
    {
        private readonly ISalaryRepository _salaryRepo;
        private readonly IOfficeRepository _officeRepo;
        private readonly IDivisionRepository _divisionRepo;
        private readonly IPositionRepository _positionRepo;
        private readonly int[] _yearMonths = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11,12 }; 

        public SalaryService(
            ISalaryRepository salaryRepo,
            IOfficeRepository officeRepo,
            IDivisionRepository divisionRepo,
            IPositionRepository positionRepo
            )
        {
            _salaryRepo = salaryRepo;
            _officeRepo = officeRepo;
            _divisionRepo = divisionRepo;
            _positionRepo = positionRepo;
        }

        public async Task<bool> AddSalariesAsync(SalaryInputDto salariesInput)
        {
            var salaries = new List<Salary>();

            foreach (var variableData in salariesInput.VariableData)
            {
                salaries.Add(new Salary
                {
                    Year = variableData.Year,
                    Month = variableData.Month,
                    BaseSalary = variableData.BaseSalary,
                    Commission = variableData.Commission,
                    CompensationBonus = variableData.CompensationBonus,
                    ProductionBonus = variableData.ProductionBonus,
                    Contributions = variableData.Contributions,
                    Grade = variableData.Grade,
                    Office = await _officeRepo.GetOfficeByIdAsync(variableData.OfficeId),
                    Position = await _positionRepo.GetPositionByIdAsync(variableData.PositionId),
                    Division = await _divisionRepo.GetDivisionByIdAsync(variableData.DivisionId),
                    EmployeeCode = salariesInput.EmployeeCode,
                    IdentificationNumber = salariesInput.IdentificationNumber,
                    EmployeeName = salariesInput.EmployeeName,
                    EmployeeSurName = salariesInput.EmployeeSurName,
                    BeginDate = salariesInput.BeginDate, 
                    Birthdate = salariesInput.Birthdate,

                });
            }
            if (await _salaryRepo.AddSalariesAsync(salaries) > 0) 
            {
                return true;
            }
            return false;  
        }

        public async Task<IEnumerable<int>> GetAvaliableMonths(AvailableMonthInputDto input)
        {
            var availableMonths = new List<int>();
            var salaries = await _salaryRepo.GetSalariesAsync();

            var usedMonths = salaries
                .Where(x =>
                x.EmployeeName.Equals(input.EmployeeName) &&
                x.EmployeeSurName.Equals(input.EmployeeSurName) &&
                x.Year == input.Year)
                .Select(x => x.Month)
                .ToList();

            for(int i = 0; i < _yearMonths.Length; i++)
            {
                if (usedMonths.Contains(_yearMonths[i])) continue;
                
                availableMonths.Add(_yearMonths[i]);
            }

            return availableMonths;
        }

        public async Task<IEnumerable<EmployeeInfoDto>> GetEployeeInfoAsync()
        {
            return await _salaryRepo.GetEployeeInfoAsync();
        }

        public async Task<IEnumerable<SalaryOutputDto>> GetSalariesAsync()
        {
            var salariesOutput = new List<SalaryOutputDto>();

            var salaries = await _salaryRepo.GetSalariesAsync();

            var lastSalariesByName = salaries
                .GroupBy(row => row.EmployeeName)
                .Select(salary => salary
                    .OrderByDescending(row => row.Year)
                        .OrderByDescending(z => z.Month)
                            .First()).ToList();


            foreach (Salary salary in lastSalariesByName)
            {
                salariesOutput.Add(new SalaryOutputDto
                {
                    EmployeeCode = salary.EmployeeCode,
                    EmployeeFullName = salary.EmployeeName +' ' + salary.EmployeeSurName, 
                    DivisionName = salary.Division.DivisionName,
                    PositionName = salary.Position.PositionName,
                    Grade = salary.Grade,
                    BeginDate = salary.BeginDate,
                    BirthDate = salary.Birthdate,
                    IdentificationNumber = salary.IdentificationNumber,
                    TotalSalary = salary.GetTotalSalary(),
                    OfficeId = salary.Office.Id

                });
            }

            return salariesOutput;
        }

    }

}
