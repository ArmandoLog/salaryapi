﻿using SalaryApi.Api.DTOs;
using SalaryApi.Domain.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace SalaryApi.Aplication
{
    public interface ISalaryService
    {
        Task<bool> AddSalariesAsync(SalaryInputDto salariesInput);
        Task<IEnumerable<EmployeeInfoDto>> GetEployeeInfoAsync();
        Task<IEnumerable<SalaryOutputDto>> GetSalariesAsync();
        Task<IEnumerable<int>> GetAvaliableMonths(AvailableMonthInputDto input);
    }
}
