using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using SalaryApi.Infrastructure;
using SalaryApi.Infrastructure.Seeding;
using System;
using System.Threading.Tasks;

namespace SalaryApi
{
    public class Program
    {
        public static async Task Main(string[] args)
        {
            //CreateHostBuilder(args).Build().Run();
            var host = CreateHostBuilder(args).Build();
            using var scope = host.Services.CreateScope();
            var services = scope.ServiceProvider;
            try {
                var context = services.GetRequiredService<DataBaseContext>();
                await context.Database.MigrateAsync();
                await Seed.SeedSalaries(context);
            }
            catch (Exception ex)
            {
                var logger = services.GetRequiredService <ILogger<Program>>();
                logger.LogError(ex,"An error has occurred during the migration");
            }

            await host.RunAsync();
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureWebHostDefaults(webBuilder =>
                {
                    webBuilder.UseStartup<Startup>();
                });
    }
}
