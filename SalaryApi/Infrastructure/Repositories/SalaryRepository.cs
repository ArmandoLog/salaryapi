﻿using Microsoft.EntityFrameworkCore;
using SalaryApi.Api.DTOs;
using SalaryApi.Domain.Entities;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SalaryApi.Infrastructure
{
    public class SalaryRepository : ISalaryRepository
    {
        private readonly DataBaseContext _context;
        public SalaryRepository(DataBaseContext context)
        {
            _context = context;
        }

        public async Task<int> AddSalariesAsync(List<Salary> salaries)
        {
            await _context.Salaries.AddRangeAsync(salaries);
            return await _context.SaveChangesAsync();
        }

        public async Task<IEnumerable<Salary>> GetSalariesAsync()
        {
            return await _context.Salaries
                .Include(s => s.Office)
                .Include(s => s.Division)
                .Include(s => s.Position)
                .ToListAsync();
        }

        public async Task<IEnumerable<EmployeeInfoDto>> GetEployeeInfoAsync()
        {
            var salaries = await _context.Salaries.ToListAsync();

            var employeesInfo = salaries
                .GroupBy(x => new
                {
                    x.EmployeeName,
                    x.EmployeeSurName,
                    x.EmployeeCode,
                    x.IdentificationNumber,
                    x.Birthdate,
                    x.BeginDate,
                }).Select(employeeInfo => new EmployeeInfoDto
                {
                    EmployeeName = employeeInfo.Key.EmployeeName,
                    EmployeeSurname = employeeInfo.Key.EmployeeSurName,
                    EmployeeCode = employeeInfo.Key.EmployeeCode,
                    IdentificationNumber = employeeInfo.Key.IdentificationNumber,
                    Birthdate = employeeInfo.Key.Birthdate,
                    BeginDate = employeeInfo.Key.BeginDate,
                }).ToList();

            return employeesInfo;
        }
    }
}
