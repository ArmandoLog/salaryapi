﻿using SalaryApi.Domain.Entities;
using System.Threading.Tasks;

namespace SalaryApi.Infrastructure
{
    public class DivisionRepository : IDivisionRepository
    {
        private readonly DataBaseContext _context;

        public DivisionRepository(DataBaseContext context)
        {
            _context = context;
        }
        public async Task<Division> GetDivisionByIdAsync(int divisionId)
        {
            return await _context.Divisions.FindAsync(divisionId);
        }
    }
}
