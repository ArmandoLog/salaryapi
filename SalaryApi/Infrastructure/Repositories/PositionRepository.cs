﻿using SalaryApi.Domain.Entities;
using System.Threading.Tasks;

namespace SalaryApi.Infrastructure
{
    public class PositionRepository : IPositionRepository
    {
        private readonly DataBaseContext _context;

        public PositionRepository(DataBaseContext context)
        {
            _context = context;
        }

        public async Task<Position> GetPositionByIdAsync(int positionId)
        {
            return await _context.Positions.FindAsync(positionId);
        }
    }
}
