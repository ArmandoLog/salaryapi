﻿using SalaryApi.Domain.Entities;
using SalaryApi.Infrastructure;
using System.Threading.Tasks;

namespace SalaryApi.Infrastructure
{
    public class OfficeRepository : IOfficeRepository
    {
        private readonly DataBaseContext _context;
        public OfficeRepository(DataBaseContext context)
        {
            _context = context;
        }

        

        public async Task<Office> GetOfficeByIdAsync(int officeId)
        {
            return await _context.Offices.FindAsync(officeId);
        }
    }
}
