﻿using SalaryApi.Domain.Entities;
using System.Threading.Tasks;

namespace SalaryApi.Infrastructure
{
    public interface IOfficeRepository
    {
        Task<Office> GetOfficeByIdAsync(int officeId);
    }
}
