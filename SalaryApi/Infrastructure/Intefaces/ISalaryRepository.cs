﻿using SalaryApi.Api.DTOs;
using SalaryApi.Domain.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace SalaryApi.Infrastructure
{
    public interface ISalaryRepository
    {
        Task<int> AddSalariesAsync(List<Salary> salaries);
        Task<IEnumerable<Salary>> GetSalariesAsync();
        Task<IEnumerable<EmployeeInfoDto>> GetEployeeInfoAsync();

    }
}
