﻿using SalaryApi.Domain.Entities;
using System.Threading.Tasks;

namespace SalaryApi.Infrastructure
{
    public interface IDivisionRepository
    {
        Task<Division> GetDivisionByIdAsync(int divisionId);

    }
}
