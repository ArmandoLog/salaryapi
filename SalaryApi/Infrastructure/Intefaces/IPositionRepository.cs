﻿using SalaryApi.Domain.Entities;
using System.Threading.Tasks;

namespace SalaryApi.Infrastructure
{
    public interface IPositionRepository
    {
        Task<Position> GetPositionByIdAsync(int positionId);
    }
}
