﻿using Microsoft.EntityFrameworkCore;
using SalaryApi.Domain.Entities;

namespace SalaryApi.Infrastructure
{
    public class DataBaseContext : DbContext        
    {
        public DataBaseContext(DbContextOptions options): base(options){}

        public DbSet<Salary> Salaries { get; set; }
        public DbSet<Office> Offices { get; set; }
        public DbSet<Division> Divisions { get; set; }
        public DbSet<Position> Positions { get; set; }
    }
}
