﻿using Microsoft.EntityFrameworkCore;
using SalaryApi.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text.Json;
using System.Threading.Tasks;

namespace SalaryApi.Infrastructure.Seeding
{
    public class Seed
    {
        public static async Task SeedSalaries(DataBaseContext context)
        {
            if (await context.Salaries.AnyAsync()) return;

            var random = new Random();
            var salaries = new List<Salary>();

            var offices = new List<Office> {
                new Office { OfficeLetter = "A"},
                new Office { OfficeLetter = "B"},
                new Office { OfficeLetter = "C"},
                new Office { OfficeLetter = "D"},
                new Office { OfficeLetter = "ZZ"},
                new Office { OfficeLetter = "Z"},
            };

            var divisions = new List<Division>{
               new Division {  DivisionName= "OPERATIONS"},
               new Division { DivisionName= "SALES" },
               new Division { DivisionName= "IT"},
               new Division {  DivisionName= "CUSTOMER CARE" },
               new Division { DivisionName= "MARKETING"}
            };

            var positions = new List<Position>{
               new Position {   PositionName= "CARGO MANAGER"},
               new Position { PositionName= "HEAD OF CARGO" },
               new Position { PositionName= "CARGO ASISTANT" },
               new Position { PositionName= "SALES MANAGER"},
               new Position {PositionName= "ACCOUNT EXECUTIVE"},
               new Position {PositionName= "PROGRAMMER"},
               new Position {  PositionName= "TECHNICAL LEADER" },
               new Position {PositionName= "CUSTOMER DIRECTOR"},
               new Position { PositionName= "CUSTOMER ASSISTANT"},
               new Position { PositionName= "MARKETING ASISTANT"},
               new Position {PositionName= "MARKETING MANAGER"}
            };



            context.Positions.AddRange(positions);
            context.Offices.AddRange(offices);
            context.Divisions.AddRange(divisions);
            await context.SaveChangesAsync(); // I do this to generate the id's


            var salariesJsonData = await System.IO.File.ReadAllTextAsync("Infrastructure/Seeding/SalarySeedData.json");
            var salariesRawData = JsonSerializer.Deserialize<List<SalaryRow>>(salariesJsonData);

            foreach (var salaryRow in salariesRawData)
            {
                for (int i = 0; i <= 5; i++)
                {
                    salaries.Add(
                            new Salary
                            {
                                Year = salaryRow.Year,
                                EmployeeCode = salaryRow.EmployeeCode.ToString(),
                                EmployeeName = salaryRow.EmployeeName,
                                EmployeeSurName = salaryRow.EmployeeSurName,
                                BeginDate = salaryRow.BeginDate,
                                Birthdate = salaryRow.Birthdate,
                                IdentificationNumber = salaryRow.IdentificationNumber.ToString(),
                                Month = salaryRow.NestedData[i].Month,
                                Office = offices[random.Next(offices.Count)],
                                Position = positions[random.Next(positions.Count)],
                                Division = divisions[random.Next(divisions.Count)],
                                Grade = salaryRow.NestedData[i].Grade,
                                BaseSalary = salaryRow.NestedData[i].BaseSalary,
                                Commission = salaryRow.NestedData[i].Commission,
                                CompensationBonus = salaryRow.NestedData[i].CompensationBonus,
                                Contributions = salaryRow.NestedData[i].Contributions,
                                ProductionBonus = salaryRow.NestedData[i].ProductionBonus,
                            }
                        );
                }
            }

            context.Salaries.AddRange(salaries);

            await context.SaveChangesAsync();
        }

        private class SalaryRow
        {
            public int Id { get; set; }
            public int Year { get; set; }
            public int EmployeeCode { get; set; }
            public string EmployeeName { get; set; }
            public string EmployeeSurName { get; set; }
            public DateTime BeginDate { get; set; }
            public DateTime Birthdate { get; set; }
            public int IdentificationNumber { get; set; }

            public NestedData[] NestedData { get; set; }
            
        }

        private class NestedData{
            public int Month { get; set; }
            public int Grade { get; set; }
            public decimal BaseSalary { get; set; }
            public decimal ProductionBonus { get; set; }
            public decimal CompensationBonus { get; set; }
            public decimal Commission { get; set; }
            public decimal Contributions { get; set; }
        }
        
    }
}
