﻿using System;
using System.ComponentModel.DataAnnotations;

namespace SalaryApi.Domain.Entities
{
    public class Salary
    {
        [Key]
        public int Id { get; set; }
        public int Year { get; set; }
        public int Month { get; set; }
        public Office Office { get; set; }

        [StringLength(10)]
        public string EmployeeCode { get; set; }

        [StringLength(150)]
        public string EmployeeName { get; set; }

        [StringLength(150)]
        public string EmployeeSurName { get; set; }
        public Division Division { get; set; }
        public Position Position { get; set; }
        public int Grade { get; set; }
        public DateTime BeginDate { get; set; }
        public DateTime Birthdate { get; set; }

        [StringLength(10)]
        public string IdentificationNumber { get; set; }
        public decimal BaseSalary { get; set; }
        public decimal ProductionBonus{ get; set; }
        public decimal CompensationBonus { get; set; }
        public decimal Commission { get; set; }
        public decimal Contributions { get; set; }

        public decimal GetTotalSalary()
        {
            var otherIncome = (BaseSalary + Commission) * (decimal)0.8 + Commission;
            return BaseSalary + ProductionBonus + CompensationBonus * (decimal)0.75 + otherIncome - Contributions;
        }
        /*I was thinkig making this logic in the salary service, but I read about the anemic anti-pattern
            and I thought it was a good chance to avoid it, giving some behavior to the entity, 
            also this is the for what I read this is a good file to place 
            a method like this one so related to the entity 
         */
    }


}
