﻿using System.ComponentModel.DataAnnotations;

namespace SalaryApi.Domain.Entities
{
    public class Position
    {
        [Key]
        public int Id { get; set; }
        public string PositionName { get; set; }
    }
}
