﻿using System.ComponentModel.DataAnnotations;

namespace SalaryApi.Domain.Entities
{
    public class Office
    {
        [Key]
        public int Id { get; set; }
        public string OfficeLetter { get; set; }
    }
}
