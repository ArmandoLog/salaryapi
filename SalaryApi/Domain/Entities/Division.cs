﻿using System.ComponentModel.DataAnnotations;

namespace SalaryApi.Domain.Entities
{
    public class Division
    {
        [Key]
        public int Id { get; set; }
        public string DivisionName { get; set; }
    }
}
